terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.59.0"
    }
  }
  backend "local" {
    path = "/Users/andrii.voloshyn/DevOps/terraform/terraform.tfstate"
  }
}

provider "aws" {
  region = "us-east-2"
  shared_credentials_file = "/Users/andrii.voloshyn/.aws/credentials"
}