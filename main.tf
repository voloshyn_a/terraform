data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

variable "tag_names" {
  type    = list(string)
  default = ["First", "Second", "Third", "Fourth"]
}

variable "instance_config" {
  type = map

  default = {
    "First" = {
        "owner" = ["nic 1", "nic 2"]
        "rbd" = []
        "it" = "t2.micro"
    }
    "Second" = {
        "owner" = []
        "rbd" = [10, 2]
        "it" = "t2.micro"
    }
    "Third" = {
        "owner" = []
        "rbd" = []
        "it" = "t2.micro"
    }
    "Fourth" = {
        "owner" = []
        "rbd" = []
        "it" = "t2.micro"
    }
  }
}

resource "aws_instance" "web" {
  for_each = var.instance_config

  ami = data.aws_ami.ubuntu.id

    dynamic "ebs_block_device" {
      for_each = each.value.rbd

      content {
        volume_size = ebs_block_device.value
        device_name = "/dev/sdf"
      }
    }

  instance_type = each.value.it

  tags = {
    Name = "Server ${each.key}"
    Owner = "Owner for ${each.key}"
  }
}